# CK Mo Portfolio Site
This site used to show my latest activities.  

## Available Scripts

### `npm start`

Runs the app in the development mode. Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm test`

Launches the test runner in the interactive watch mode.

### `npm run build`

Builds the app for production to the `build` folder.

### `npm run lint`

Run Eslint to statically analyzes project code 
