import React from 'react';
import renderer from 'react-test-renderer';
import Mobile from './index';
import 'jest-styled-components';

test('render App', () => {
  const tree = renderer.create(<Mobile />).toJSON();

  expect(tree).toMatchSnapshot();
});
