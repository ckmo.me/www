import React from 'react';
import renderer from 'react-test-renderer';
import { Provider } from 'react-redux';
import Home from './index';
import 'jest-styled-components';
import store from '../../store';

test('render App', () => {
  const tree = renderer.create(
    <Provider store={store}>
      <Home />
    </Provider>,
  ).toJSON();

  expect(tree).toMatchSnapshot();
});
